from typing import Any, ClassVar, List, Optional, Union

import pytest

from settingscascade import ElementSchema, SettingsManager


def test_use_schema_to_view_settings():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        basic: str

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                "task": {"someval": "override"},
            }
        ],
        [TaskSchema],
    )

    assert config.basic == "Var default"
    assert config.someval == "default"
    assert config.task().someval == "override"


def test_use_schema_with_id_to_view_settings():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        basic: str

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                "task#mytask": {"someval": "override"},
            }
        ],
        [TaskSchema],
    )

    assert config.task(identifier="mytask").someval == "override"


def test_use_schema_with_name_to_view_settings():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        basic: str

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                "task.mytask": {"someval": "override"},
            }
        ],
        [TaskSchema],
    )

    assert config.task(name="mytask").someval == "override"


def test_use_schema_with_multiple_classes():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        basic: str

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                ".secondtask": {"someval": "second"},
                "task.mytask.secondtask": {"someval": "override"},
            }
        ],
        [TaskSchema],
    )

    assert config.task(name="mytask.secondtask").someval == "override"
    assert config.task(name="mytask").someval == "default"
    assert config.task(name="secondtask").someval == "second"


def test_nested_schema():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        basic: str

    class Env(ElementSchema):
        _name_ = "env"

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                "task": {"someval": "override"},
                "env": {"task": {"someval": "nested"}},
            }
        ],
        [TaskSchema, Env],
    )
    with config.context("env"):
        assert config.task().someval == "nested"


def test_schema_validates_missing_key_data_load():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        basic: str

    data = {
        "basic": "Var {{someval}}",
        "someval": "default",
        "task.mytask": {"someval": "override", "badval": "bad"},
    }

    with pytest.raises(ValueError):
        SettingsManager([data], [TaskSchema])


def test_schema_allows_missing_key_data_load_if_specified():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        _allowextra_ = True
        someval: str
        basic: str

    data = {
        "basic": "Var {{someval}}",
        "someval": "default",
        "task.mytask": {"someval": "override", "badval": "bad"},
    }

    SettingsManager([data], [TaskSchema])


def test_schema_validates_wrong_type_data_load():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        badval: str

    data = {
        "basic": "Var {{someval}}",
        "someval": "default",
        "task.mytask": {"someval": "override", "badval": 7},
    }

    with pytest.raises(TypeError):
        SettingsManager([data], [TaskSchema])


def test_schema_validates_generics():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: List[str]
        basic: List
        anyval: Any
        opt: Optional[str]

    bad_data = {
        "basic": "Var {{someval}}",
        "someval": "default",
        "task.mytask": {"someval": ["hi"], "basic": "yo"},
    }

    good_data = {
        "basic": "Var {{someval}}",
        "someval": "default",
        "task.mytask": {"someval": ["hi"], "basic": ["yo"], "anyval": 77, "opt": None},
    }

    SettingsManager([good_data], [TaskSchema])
    with pytest.raises(TypeError):
        SettingsManager([bad_data], [TaskSchema])


def test_schema_validates_union():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: Union[list, str]

    bad_data = {"task.mytask": {"someval": 42}}

    good_data_first = {"task.mytask": {"someval": ["hi"]}}
    good_data_second = {"task.mytask": {"someval": "hi"}}

    SettingsManager([good_data_first, good_data_second], [TaskSchema])
    with pytest.raises(TypeError):
        SettingsManager([bad_data], [TaskSchema])


def test_schema_validates_classvar():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: List[str]
        classvar: ClassVar = 3

    bad_data = {
        "basic": "Var {{someval}}",
        "task.mytask": {"someval": ["hi"], "classvar": "anything"},
    }

    good_data = {
        "basic": "Var {{someval}}",
        "someval": "default",
        "task.mytask": {"someval": ["hi"]},
    }

    config = SettingsManager([good_data], [TaskSchema])
    assert config.task(name="mytask").classvar == 3
    with pytest.raises(ValueError):
        SettingsManager([bad_data], [TaskSchema])


def test_schema_dump():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str
        basic: str

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                "otherval": "should not show up",
                "task.mytask": {"someval": "override"},
            }
        ],
        [TaskSchema],
    )

    assert config.task(name="mytask").load() == {
        "someval": "override",
        "basic": "Var override",
    }
