import pytest

from settingscascade import ElementSchema, SettingsManager


def test_value_from_base(config):
    with config.context():
        assert config.base_location == ".rye"


def test_get_from_environment(config):
    with config.context("environment.py36"):
        assert config.python == "python3.6"


def test_push_config(config):
    config.push_context("environment.py36")
    assert config.python == "python3.6"
    config.pop_context()
    with pytest.raises(ValueError):
        assert config.python


def test_can_override_environment_from_task(config):
    with config.context("environment.py36"):
        assert config.python == "python3.6"
    with config.context("task.test environment.py36"):
        assert config.python == "python3.2"


def test_can_fallback_to_less_specific_environment(config):
    with config.context("environment.py36"):
        assert config.location == [".rye", "py36"]
    with config.context("task.test environment.py36"):
        assert config.location == [".rye", "py36"]


def test_can_get_default_value(config):
    with pytest.raises(ValueError):
        assert config.default_value == "some_value"
    with config.context("environment.py36"):
        assert config.default_value == "some_value"
    with config.context("environment.py37"):
        assert config.default_value == "some_value"


def test_bad_config():
    # Tries to specify a tag used as a level for a value
    class Tag(ElementSchema):
        pass

    with pytest.raises(TypeError):
        SettingsManager([{"tag": 3}], [Tag])


def test_nested_map():
    class Tag(ElementSchema):
        nest: str

    class Nest(ElementSchema):
        val: str

    config = SettingsManager([{"tag": {"nest": {"val": "1"}}}], [Tag, Nest])
    with config.context("tag nest"):
        assert config.val == "1"


def test_rejects_invalid_schema():
    class Tag:
        val: str

    with pytest.raises(TypeError):
        SettingsManager([{}], [Tag])


def test_can_resolve_ints():
    config = SettingsManager([{"val": 42}], [])
    assert config.val == 42


def test_pop_empty_context():
    config = SettingsManager([{"val": 42}], [])
    config.push_context(".hello")
    config.pop_context()
    config.pop_context()


def test_load_bare_class():
    config = SettingsManager([{"val": 11, ".class": {"val": 42}}], [])
    assert config.val == 11
    with config.context(".class"):
        assert config.val == 42
