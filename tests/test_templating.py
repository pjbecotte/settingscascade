from settingscascade import ElementSchema
from settingscascade.manager import SettingsManager


def test_optional_self_reference():
    class Tag(ElementSchema):
        val: str

    def join(args):
        sep, *terms = args
        return sep.join([term for term in terms if term])

    config = SettingsManager([{"tag": {"val": "{{('_', name, id) | join }}"}}], [Tag])

    config.add_filter("join", join)
    assert config.tag(name="mytag").val == "mytag"
    assert config.tag(identifier="myid").val == "myid"
    assert config.tag(name="othertag", identifier="otherid").val == "othertag_otherid"


def test_self_reference():
    class Tag(ElementSchema):
        val: str
        other: str

    config = SettingsManager(
        [{"tag": {"val": "{{name}} is the tag name", "other": "{{id}} is the tag id"}}],
        [Tag],
    )
    assert config.tag(name="mytag").val == "mytag is the tag name"
    assert config.tag(identifier="myid").other == "myid is the tag id"


def test_layered_config():
    class TaskSchema(ElementSchema):
        _name_ = "task"
        someval: str

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                "task": {"someval": "override"},
            },
            {"someval": "newer", "newval": "from second"},
        ],
        [TaskSchema],
    )

    assert config.basic == "Var newer"
    with config.context("task"):
        assert config.basic == "Var override"


def test_get_value_stack(config):
    dat = list(config.return_value_stack("task.test"))
    assert str(dat[0]) == ("task.test: commands=[['pytest', 'tests']]")
    assert str(dat[1]) == ("task: isolate=False")
    assert str(dat[2]) == (
        ": base_location=.rye, original=the first, "
        "derived=derived is {{ original }}, "
        "filtered=This is multiplied 2 * 2 = {{ (2, 2) | mult }}, "
        "filtered_list=['hello', '{{ original }}']"
    )


def test_custom_filter(config):
    config.add_filter("mult", lambda a: a[0] * a[1])
    assert config.filtered == "This is multiplied 2 * 2 = 4"


def test_define_filter():
    config = SettingsManager({"myval": "{{ (1, 3) | add_two_numbers }}"})
    config.add_filter("add_two_numbers", lambda tup: tup[0] + tup[1])
    assert config.myval == "4"


def test_readme_example():
    class Task(ElementSchema):
        someval: str

    config = SettingsManager(
        [
            {
                "basic": "Var {{someval}}",
                "someval": "default",
                "task": {"someval": "override"},
            }
        ],
        [Task],
    )

    assert config.basic == "Var default"
    with config.context("task"):
        assert config.basic == "Var override"


def test_template_value(config):
    assert config.derived == "derived is the first"


def test_template_list(config):
    assert config.filtered_list == ["hello", "the first"]


def test_refer_to_other_context():
    class Tag(ElementSchema):
        val: str

    config = SettingsManager(
        [
            {
                "tag.mytag": {"val": "{{ config.with_context('.other').val }}"},
                ".other": {"val": "40"},
            }
        ],
        [Tag],
    )
    assert config.with_context(".other").val == "40"
    assert config.tag(name="mytag").val == "40"


def test_refer_to_parent_element():
    class Tag(ElementSchema):
        val: str

    class Child(ElementSchema):
        val: str

    config = SettingsManager(
        [
            {
                "tag.mytag": {"val": "hi"},
                "child": {"val": "{{config.parent().val + 'yo'}}"},
            }
        ],
        [Tag, Child],
    )

    with config.context("tag.mytag child"):
        assert config.parent().val == "hi"
        assert config.val == "hiyo"
