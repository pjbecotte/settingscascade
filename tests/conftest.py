import pytest
from yaml import safe_load

from settingscascade import ElementSchema
from settingscascade.manager import SettingsManager

updated_testdata = """
settings:
    base_location: .rye
    original: the first
    derived: derived is {{ original }}
    filtered: This is multiplied 2 * 2 = {{ (2, 2) | mult }}
    filtered_list:
        - hello
        - "{{ original }}"

    task.test:
        commands:
            - ["pytest", "tests"]
    task.lint:
        commands:
            - ["pylint", "contractual"]
    task:
        isolate: false
        environment.py36:
            python: python3.2
    environment.py37:
        python: python3.7
        location:
            - "{{ base_location }}"
            - py37
    environment.py36:
        python: python3.6
        location:
            - "{{ base_location }}"
            - py36
    environment:
        default_value: some_value
        depends_files:
            - pyproject.toml
            - poetry.lock
        setup_command:
            - poetry
            - install

configs:
    default:
        task(id=test):
            - environment(class=py36)
            - environment(class=py37)
        task(id=lint):
            - environment(class=py37)
"""


@pytest.fixture(scope="session")
def global_config():
    class Task(ElementSchema):
        _name_ = "task"
        isolate: bool
        target_environments: list
        commands: list

    class Environment(ElementSchema):
        _name_ = "environment"
        default_value: str
        python: str
        depends_files: list
        location: list
        setup_command: list

    return SettingsManager(
        [safe_load(updated_testdata)["settings"]], [Task, Environment]
    )


@pytest.fixture
def config(global_config):  # pylint: disable=redefined-outer-name
    yield global_config
    global_config.clear_context()
