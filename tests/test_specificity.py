# pylint: disable=redefined-outer-name
from unittest.mock import Mock

import pytest

from settingscascade.selector import Selector, SelectorStorage


def test_el():
    assert Selector("el").specificity == (0, 0, 1)


def test_class():
    assert Selector(".class").specificity == (0, 1, 0)


def test_id():
    assert Selector("#id").specificity == (1, 0, 0)


def test_multi():
    assert Selector("jon.wayne #mydude .class").specificity == (1, 2, 1)


def test_gt():
    assert Selector("#id") > Selector("el.class")


def test_chained_classes():
    assert Selector("el.class1.class2").specificity == (0, 2, 1)


def test_multi_level_gt():
    assert Selector("#you .class") > Selector("hi.class")


def test_negative():
    assert -Selector("#you .class").specificity < -Selector("hi.class").specificity


def test_matches():
    assert Selector("el.class").matches("el.class") is True


def test_cant_match_up_a_chain():
    assert Selector("el.class .class2").matches("el.class") is False


def test_cant_match_less_specific():
    assert Selector("el.class .class2").matches(".class2") is False


def test_assert_unspecified_class():
    assert Selector(".class").matches("el.class") is True


def test_matches_multiple_terms():
    assert Selector("el.class .class").matches(".top el.class p.class")


def ruleset(selector):
    return Mock(selector=Selector(selector), order=0)


@pytest.fixture()
def check():
    store = SelectorStorage()
    store.add(ruleset("hi.class"))
    store.add(ruleset(".class"))
    store.add(ruleset("#you .class"))
    store.add(ruleset("#it el.class hi.otherclass"))
    store.add(ruleset("el hi.class"))
    store.add(ruleset(".class #id"))
    store.add(ruleset("hi"))
    store.add(ruleset("#other .class .class2"))
    store.add(ruleset("top bottom"))

    def check(to_match, expected):
        looked_up = next(store.lookup_rules(to_match)).selector
        assert looked_up.text == expected

    return check


def test_lookup(check):
    check("body el.high hi.class", "el hi.class")


def test_high_specificity(check):
    check("#it el.class hi.otherclass", "#it el.class hi.otherclass")


def test_only_class(check):
    check("bob.class", ".class")


def test_only_el(check):
    check("hi.blah", "hi")


def test_higher_specificity_at_next_level(check):
    check("#you hi.class", "#you .class")


def test_complex_uptree(check):
    check("#other div.class #id.class2", "#other .class .class2")


def test_inheritance(check):
    check(".class badelement", ".class")


def test_class_uptree_vs_el(check):
    check("top.class bottom", "top bottom")


def test_no_match(check):
    with pytest.raises(StopIteration):
        check("this.doesnt.exist", "nothing")
