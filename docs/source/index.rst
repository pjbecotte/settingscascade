.. include:: ../../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Documentation

   loading_data
   reading_values
   templating
   api
